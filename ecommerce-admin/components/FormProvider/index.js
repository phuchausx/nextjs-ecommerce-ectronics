import { FormContext } from '@/utils/constants';
import { Form } from "antd";

export default function FormProvider({children}) {
   const [form] = Form.useForm();

   return (
      <FormContext.Provider 
         value={{
            form,
         }}
      >
         {children}
      </FormContext.Provider>
   )
}
