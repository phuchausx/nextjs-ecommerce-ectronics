import { WrapLogin } from "./styles";
import { Button } from "antd";

export default function Login({
   onClick,
}) {

   return (
      <WrapLogin>
         <Button size="large" onClick={onClick}>
            Login with Google
         </Button>
      </WrapLogin>
   )
}