import Link from "next/link";
import iconLogo from '@/assets/icons/ic-logo.svg';
import { DashboardOutlined, ShoppingCartOutlined, LaptopOutlined, SettingOutlined } from '@ant-design/icons';
import { WrapNav } from './styles';
import { useRouter } from "next/router";

export default function Nav() {
   const router = useRouter();

   return (
      <WrapNav>
         <div className="nav-logo">
            <Link href="/">
               <img src={iconLogo.src} width="80px" alt="Logo ecommerce" />
            </Link>
         </div>
         <nav className="nav-menu">
            <Link href="/" className={`nav-item ${router.pathname === '/' && 'nav-item-active'}`}>
               <DashboardOutlined />
               <span>
                  Dashboard
               </span>
            </Link>
            <Link href="/products" className={`nav-item ${router.pathname.includes('products') && 'nav-item-active'}`}>
               <LaptopOutlined />
               <span>
                  Products
               </span>
            </Link>
            <Link href="/orders" className={`nav-item ${router.pathname.includes('orders') && 'nav-item-active'}`}>
               <ShoppingCartOutlined />
               <span>
                  Orders
               </span>
            </Link>
            <Link href="/settings" className={`nav-item ${router.pathname.includes('settings') && 'nav-item-active'}`}>
               <SettingOutlined />
               <span>
                  Settings
               </span>
            </Link>
         </nav>
      </WrapNav>
   )
}