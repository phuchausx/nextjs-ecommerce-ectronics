import { styled } from "styled-components";

export const WrapNav = styled.div`
   flex: 0 0 17%;
   display: flex;
   flex-direction: column;
   align-items: center;

   .nav-logo {
      text-align: center;
   }

   .nav-menu {
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 100%;
      color: #fff;
   }

   .nav-item {
      padding: 15px 10px;
      width: 100%;

      span {
         margin-left: 12px;
      }

      &:hover {
         background: #4096ff;
      }

      &-active {
         background: #f9f8f8;
         color: #4096ff;

         &:hover {
         background: #f9f8f8;
      }
      }
   }
`