import { ProductFormStyle } from './styles';
import { useRouter } from "next/router";
import { Card, Col, Form, Input, Row, Space, Button } from "antd";

export default function ProductForm({form, handleSubmit}) {
   const router = useRouter();

   return (
      <ProductFormStyle>
         <Card title="Product Information" bordered={false}>
            <Form
               form={form}
               name="validateOnly"
               layout="vertical"
               onFinish={handleSubmit}
            >
               <Row gutter={[16, 0]}>
                  <Col md={12} sm={24} xs={24}>
                     <Form.Item
                        name="name"
                        label="Product Name"
                        rules={[{ required: true, message: 'Please input your Name!' }]}
                     >
                        <Input />
                     </Form.Item>
                  </Col>
                  <Col md={12} sm={24} xs={24}>
                     <Form.Item
                        name="price"
                        label="Price"
                        rules={[{ required: true, message: 'Please input your Price!' }]}
                     >
                        <Input type="number" />
                     </Form.Item>
                  </Col>
                  <Col md={12} sm={24} xs={24}>
                     <Form.Item
                        name="description"
                        label="Description"
                     >
                        <Input.TextArea rows={4} style={{ resize: 'none' }} />
                     </Form.Item>
                  </Col>
               </Row>
            </Form>
         </Card>
         <Space className="productForm-actionBtn" size={10}>
            <Button htmlType="submit" type="primary" onClick={() => form.submit()}>Save</Button>
            <Button onClick={() => router.back()}>Cancel</Button>
         </Space>
      </ProductFormStyle>
   )
}
