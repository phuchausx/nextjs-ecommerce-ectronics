import { styled } from "styled-components";

export const ProductFormStyle = styled.div`
   .productForm {
      &-actionBtn {
         justify-content: flex-end;
         margin-top: 20px;
         width: 100%;
      }
   }
`;