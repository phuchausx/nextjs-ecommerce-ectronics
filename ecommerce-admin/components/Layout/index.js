// import { Inter } from 'next/font/google'
// import styles from '@/styles/Home.module.css'
import { WrapLayout } from './styles';
import Login from '@/components/Login';
import { useSession, signIn, signOut } from "next-auth/react"
import Nav from '@/components/Nav';
import FormProvider from '@/components/FormProvider';

// const inter = Inter({ subsets: ['latin'] })

export default function Layout({ children }) {
  const { data: session } = useSession();

  if (!session) {
    return (
      <WrapLayout>
        <Login onClick={() => signIn("google")} />
      </WrapLayout>
    )
  }

  return (
    <WrapLayout>
      <FormProvider>
        <Nav />
        <div className="wrap-dummy">
         {children}
        </div>
      </FormProvider>
    </WrapLayout>
  )
}
