import { styled } from "styled-components";

export const WrapLayout = styled.div`
   height: 100vh;
   width: 100vw;
   background-color: var(--blue-8);
   display: flex;

   .wrap-dummy {
      flex-grow: 1;
      background-color: #f9f8f8;
      margin: 15px 15px 15px 0;
      padding: 15px;
      border-radius: 10px;
   }
`