import { useSession } from "next-auth/react";
import { HomeStyle } from "./styles";

export default function Home() {
  const { data: session } = useSession();
  return (
    <HomeStyle>
      <div className="flex-between">
        <h2>
        {`Hello, ${session?.user?.name}`}
        </h2>
        <img
          src={session?.user?.image}
          alt="avatar" width="50px"
          height="50px"
          className="home-avatar-image"
        />
      </div>
    </HomeStyle>
  )
}
