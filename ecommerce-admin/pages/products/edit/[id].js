import { EditProductStyle } from "./styles";
import { useEffect,useContext } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { FormContext } from '@/utils/constants';
import ProductForm from "@/components/ProductForm";

export default function EditProductPage() {
   const { form } = useContext(FormContext);
   const router = useRouter()
   const { id } = router.query;

   useEffect(() => {
      if (id && form) {
         console.log('aaaa');
         axios.get(`/api/products?id=${id}`).then(response => {
            const { name, price, description } = response.data;
            console.log(name, price, description);
            form.setFieldsValue({ name: name, price: price, description: description });
         })
      }
   }, [id, form]);

   const handleSubmit = async (value) => {
      await axios.put('/api/products', { ...value, _id: id });
      router.back();
   }

   return (
      <EditProductStyle>
         <ProductForm form={form} handleSubmit={handleSubmit} />
      </EditProductStyle>
   )
}