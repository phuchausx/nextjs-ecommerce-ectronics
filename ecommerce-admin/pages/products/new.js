import axios from "axios";
import ProductForm from "@/components/ProductForm";
import { useContext } from "react";
import { FormContext } from "@/utils/constants";
import { useRouter } from "next/router";

export default function NewProduct() {
   const router = useRouter();
   const { form } = useContext(FormContext);

   const handleSubmit = async (value) => {
      await axios.post('/api/products', value);
      form.resetFields();
      router.back();
   }

   return (
      <div>
         <ProductForm handleSubmit={handleSubmit} form={form} />
      </div>
   )
}