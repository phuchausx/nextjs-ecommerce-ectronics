import { styled } from "styled-components";

export const HomeStyle = styled.div`
   .home {
      &-avatar {
         &-image {
            padding: 8px;
            background: #e5dfdf;
            border-radius: 10px;
         }
      }
   }
`

export const ProductsStyle = styled.div`
   .products {
      &-editIcon {
         color: #69b1ff;
         cursor: pointer;
      }

      &-deleteIcon {
         color: #cf1322;
         cursor: pointer;
      }
   }
`