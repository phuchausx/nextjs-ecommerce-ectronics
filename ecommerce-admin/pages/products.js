import { Button, Table, Column, Space, Modal } from "antd";
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { ProductsStyle } from "./styles";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";

export default function Products() {
   const router = useRouter();
   const [productList, setProductList] = useState([]);
   const [deleteLoading, setDeleteLoading] = useState(false);
   const [openModal, setOpenModal] = useState(false);
   const [idCurrent, setIdCurrent] = useState(0);
   const [reloadProducts, setReloadProducts] = useState(false);

   const handleDelete = async () => {
      setDeleteLoading(true);

      await axios.delete(`/api/products?id=${idCurrent}`);
      setOpenModal(false);
      setDeleteLoading(false);
      setReloadProducts(!reloadProducts);
    };

   useEffect(() => {
      console.log('hello');
      axios.get('/api/products').then(response => setProductList(response.data));
   }, [reloadProducts]);

   console.log('productList', productList);

   return (
      <ProductsStyle>
         <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={() => router.push('/products/new')}
         >
            Add new
         </Button>

         <div className="mt-15">
            <Table bordered dataSource={productList}>
               <Column
                  title="Name"
                  dataIndex="name"
                  key="name"
                  sorter={{
                     compare: (a, b) => a.name - b.name,
                     multiple: 2,
                  }}
               />
               <Column
                  title="Price"
                  dataIndex="price"
                  key="price"
                  sorter={{
                     compare: (a, b) => a.price - b.price,
                     multiple: 1,
                  }}
               />
               <Column title="Description" dataIndex="description" key="description" />
               <Column
                  title="Action"
                  key="action"
                  render={(data) => {
                     return (
                        <Space size={15}>
                           <EditOutlined className="products-editIcon" onClick={() => router.push(`/products/edit/${data._id}`)} />
                           <DeleteOutlined className="products-deleteIcon" onClick={() => {
                              setIdCurrent(data._id);
                              setOpenModal(true);
                           }} />
                        </Space>
                        )
                  }}
               />
            </Table>
         </div>
         <Modal
            title="Confirm"
            open={openModal}
            onOk={handleDelete}
            confirmLoading={deleteLoading}
            onCancel={() => setOpenModal(false)}
         >
            <p>Are you sure you want to delete this product?</p>
         </Modal>
      </ProductsStyle>
   )
}